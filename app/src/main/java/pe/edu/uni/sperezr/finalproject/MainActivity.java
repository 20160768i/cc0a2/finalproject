package pe.edu.uni.sperezr.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bosphere.filelogger.FL;
import com.bosphere.filelogger.FLConfig;
import com.bosphere.filelogger.FLConst;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    TextView textViewQuestion, textViewNumberOfQuestion;
    ImageView imageViewQuestion;
    Button buttonPrevious, buttonNext, buttonTrue, buttonFalse;
    RelativeLayout relativeLayout;

    ArrayList<Question> questionList = new ArrayList<>();
    int length = 10;
    Question currentQuestion;
    int indexOfCurrentQuestion;
    String numberOfCurrentQuestion;
    Boolean answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //FileLogger
        FL.init(new FLConfig.Builder(this)
                .minLevel(FLConst.Level.V)
                .logToFile(true)
                .dir(new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "quiz_project_log"))
                .retentionPolicy(FLConst.RetentionPolicy.FILE_COUNT)
                .build());
        FL.setEnabled(true);
        FL.i(TAG, "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generateQuestions();
        textViewQuestion = findViewById(R.id.text_view_question);
        textViewQuestion.setText(currentQuestion.getQuestion());
        textViewNumberOfQuestion = findViewById(R.id.text_view_number_of_question);
        textViewNumberOfQuestion.setText(numberOfCurrentQuestion);
        imageViewQuestion = findViewById(R.id.image_view_question);
        imageViewQuestion.setImageResource(currentQuestion.getImg());
        buttonPrevious = findViewById(R.id.button_previous);
        buttonNext = findViewById(R.id.button_next);
        buttonTrue = findViewById(R.id.button_true);
        buttonFalse = findViewById(R.id.button_false);
        relativeLayout = findViewById(R.id.relative_layout);

        //In Previous: show the previous question or stuck on the first question
        buttonPrevious.setOnClickListener(v -> {
            FL.i(TAG, "onButtonPrevious");
            if(indexOfCurrentQuestion == 0) {
                return;
            }
            indexOfCurrentQuestion--;
            updateQuestion();
        });

        //In Next: show the next question or return to first question
        buttonNext.setOnClickListener(v -> {
            FL.i(TAG, "onButtonNex");
            if(indexOfCurrentQuestion == length - 1){
                indexOfCurrentQuestion = 0;
                updateQuestion();
                return;
            }
            indexOfCurrentQuestion++;
            updateQuestion();
        });

        //In True or False: Show a SnackBar
        //True button
        buttonTrue.setOnClickListener(v -> {
            FL.i(TAG, "onButtonTrue");
            if(answer) {
                Snackbar.make(relativeLayout, R.string.snack_bar_msg_correct, Snackbar.LENGTH_SHORT).show();
            }else{
                Snackbar.make(relativeLayout, R.string.snack_bar_msg_incorrect, Snackbar.LENGTH_SHORT).show();
            }
        });

        //False button
        buttonFalse.setOnClickListener(v -> {
            FL.i(TAG, "onButtonFalse");
            if(!answer) {
                Snackbar.make(relativeLayout, R.string.snack_bar_msg_correct, Snackbar.LENGTH_SHORT).show();
            }else{
                Snackbar.make(relativeLayout, R.string.snack_bar_msg_incorrect, Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    private void updateQuestion(){
        currentQuestion = questionList.get(indexOfCurrentQuestion);
        numberOfCurrentQuestion = getResources().getString(R.string.question, (indexOfCurrentQuestion + 1));

        textViewQuestion.setText(currentQuestion.getQuestion());
        textViewNumberOfQuestion.setText(numberOfCurrentQuestion);
        answer = currentQuestion.getAnswer();
        imageViewQuestion.setImageResource(currentQuestion.getImg());
    }

    private void generateQuestions(){
        String[] questionListFromResources = getResources().getStringArray(R.array.questions);
        String[] answersListFormResources = getResources().getStringArray(R.array.answers);
        for(int i = 0; i < length; i++){
            questionList.add(new Question(questionListFromResources[i],
                    Boolean.parseBoolean(answersListFormResources[i]),
                    R.drawable.ic_launcher_background));
        }

        indexOfCurrentQuestion = 0;
        currentQuestion = questionList.get(0);
        answer = questionList.get(0).getAnswer();
        numberOfCurrentQuestion = getResources().getString(R.string.question, 1);
    }
}