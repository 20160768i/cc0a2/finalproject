package pe.edu.uni.sperezr.finalproject;

public class Question {
    private String question;
    private boolean answer;
    private int img;

    public Question(String question, boolean answer, int img) {
        this.question = question;
        this.answer = answer;
        this.img = img;
    }

    public String getQuestion() {
        return question;
    }

    public boolean getAnswer() {
        return answer;
    }

    public int getImg() {
        return img;
    }
}
